package com.eordie.inventory.api;

import com.eordie.inventory.model.commons.Page;
import com.eordie.inventory.model.product.Sku;
import com.eordie.inventory.model.product.SkuProto;
import com.eordie.inventory.service.SkuService;
import com.eordie.inventory.utils.Base64Deserializer;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.UUID;

import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

@Validated
@RestController
@RequestMapping("/skus")
@RequiredArgsConstructor
@PreAuthorize("hasAnyAuthority('sku:read', 'sku:write')")
public class SkuController {

    private final SkuService service;

    @GetMapping
    @PreAuthorize("hasAuthority('sku:read')")
    public Mono<Page<Sku, SkuCursor>> find(
            @RequestParam(name = "name", required = false) String name,
            @RequestParam(name = "brand", required = false) String brand,
            @RequestParam(name = "quantity[lte]", required = false) BigDecimal quantityLte,
            @RequestParam(name = "page_size", required = false, defaultValue = "100") int pageSize,
            @RequestParam(name = "cursor", required = false) String rawValue) {

        SkuCursor actualCursor = Base64Deserializer.deserialize(rawValue, SkuCursor.class)
                .orElseGet(() -> new SkuCursor(name, brand, quantityLte, pageSize));

        return service.findPage(actualCursor);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('sku:write')")
    public Flux<Sku> create(@Valid @RequestBody Flux<Sku> entities) {
        return service.save(entities);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('sku:read')")
    public Mono<ResponseEntity<Sku>> findOne(@PathVariable("id") UUID id) {
        return service.findOne(id)
                .map(it -> ok().body(it))
                .defaultIfEmpty(notFound().build());
    }

    @PatchMapping("/{id}")
    @PreAuthorize("hasAuthority('sku:write')")
    public Mono<ResponseEntity<Sku>> updateOne(@PathVariable("id") UUID id, @Validated(SkuProto.Update.class) @RequestBody Mono<SkuProto> prototype) {
        Mono<Sku> mono = service.findOne(id)
                .then(prototype.map(it -> new Sku(id, it)));

        return service.updateOne(mono)
               .map(it -> ok().body(it))
               .defaultIfEmpty(notFound().build());
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('sku:write')")
    public Mono<Sku> replaceOne(@PathVariable("id") UUID id, @Validated(SkuProto.Update.class) @RequestBody Mono<SkuProto> prototype) {
        Flux<Sku> input = Flux.from(prototype.map(it -> new Sku(id, it)));

        return service.save(input).single();
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('sku:write')")
    public Mono<ResponseEntity<Void>> delete(@PathVariable("id") UUID id) {
        return service.findOne(id)
                .flatMap(it -> service.delete(it.getId())
                        .then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT))))
                .defaultIfEmpty(notFound().build());
    }
}
