package com.eordie.inventory.api;

import com.eordie.inventory.model.commons.Cursor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SkuCursor implements Cursor {

    private String name;
    private String brand;
    private BigDecimal quantityLte;
    private int pageSize;
    private LocalDateTime createdAt;

    public SkuCursor(String name, String brand, BigDecimal quantityLte, int pageSize) {
        this(name, brand, quantityLte, pageSize, LocalDateTime.ofEpochSecond(0, 0, ZoneOffset.UTC));
    }

    @Override
    public void moveWindow(LocalDateTime dateTime) {
        this.createdAt = dateTime;
    }
}
