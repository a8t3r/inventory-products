package com.eordie.inventory.config;

import com.eordie.inventory.config.internal.decoder.CsvHttpMessageDecoder;
import com.eordie.inventory.config.internal.decoder.XlsHttpMessageDecoder;
import com.eordie.inventory.config.internal.decoder.XlsxHttpMessageDecoder;
import com.eordie.inventory.config.internal.encoder.CsvHttpMessageEncoder;
import com.eordie.inventory.config.internal.encoder.XlsHttpMessageEncoder;
import com.eordie.inventory.config.internal.encoder.XlsxHttpMessageEncoder;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import org.springframework.boot.web.codec.CodecCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.codec.CodecConfigurer;

@Configuration
public class ContentTypeNegotiationConfig {

    public static final ObjectMapper DEFAULT_MAPPER = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .setDefaultPropertyInclusion(JsonInclude.Include.NON_NULL)
            .setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE)
            .findAndRegisterModules();

    @Bean
    public ObjectMapper objectMapper() {
        return DEFAULT_MAPPER;
    }

    @Bean
    public CodecCustomizer codecCustomizer() {
        ObjectMapper mapper = new ObjectMapper();
        return configurer -> {
            CodecConfigurer.CustomCodecs codecs = configurer.customCodecs();
            codecs.decoder(new XlsHttpMessageDecoder<>(mapper));
            codecs.decoder(new XlsxHttpMessageDecoder<>(mapper));
            codecs.decoder(new CsvHttpMessageDecoder<>(mapper));

            codecs.encoder(new CsvHttpMessageEncoder<>());
            codecs.encoder(new XlsHttpMessageEncoder<>());
            codecs.encoder(new XlsxHttpMessageEncoder<>());
        };
    }
}
