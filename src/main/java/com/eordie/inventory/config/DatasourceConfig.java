package com.eordie.inventory.config;

import com.eordie.inventory.utils.LocalDateTimeConverter;
import com.github.pgasync.ConnectionPoolBuilder;
import com.github.pgasync.Db;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URI;

import static org.apache.commons.lang3.StringUtils.substringAfter;

@Configuration
public class DatasourceConfig {

    @Bean
    public Db db(DataSourceProperties properties) {
        URI uri = URI.create(substringAfter(properties.getUrl(), ":"));
        return new ConnectionPoolBuilder()
                .hostname(uri.getHost())
                .port(uri.getPort())
                .database(substringAfter(uri.getPath(), "/"))
                .username(properties.getUsername())
                .password(properties.getPassword())
                .converters(new LocalDateTimeConverter())
                .poolSize(20)
                .build();
    }
}
