package com.eordie.inventory.config.internal;

import org.springframework.http.MediaType;

public class CustomMediaTypes {

    public static final MediaType TEXT_CSV = MediaType.parseMediaType("text/csv");
    public static final MediaType APPLICATION_EXCEL = MediaType.parseMediaType("application/vnd.ms-excel");
    public static final MediaType APPLICATION_EXCEL_X = MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

}
