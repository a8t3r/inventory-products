package com.eordie.inventory.config.internal.decoder;

import com.coreoz.windmill.files.FileSource;
import com.coreoz.windmill.imports.FileParser;
import com.coreoz.windmill.imports.Row;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.reactivestreams.Publisher;
import org.springframework.core.ResolvableType;
import org.springframework.core.codec.AbstractDecoder;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.util.MimeType;
import reactor.core.publisher.Flux;

import java.util.Map;
import java.util.Objects;

public abstract class AbstractRowDecoder<T> extends AbstractDecoder<T> {

    private final FileParser parser;
    private final ObjectMapper mapper;
    
    public AbstractRowDecoder(FileParser parser, ObjectMapper mapper, MimeType... supportedMimeTypes) {
        super(supportedMimeTypes);
        this.parser = parser;
        this.mapper = mapper;
    }

    @Override
    public Flux<T> decode(Publisher<DataBuffer> inputStream, ResolvableType elementType, MimeType mimeType, Map<String, Object> hints) {
        return Flux.from(DataBufferUtils.join(inputStream))
                .flatMap(it -> Flux.fromStream(parser.parse(FileSource.of(it.asInputStream()))))
                .skip(1)
                .map(row -> {
                    try {
                        return instantiate(elementType, row);
                    } catch (ReflectiveOperationException e) {
                        throw new IllegalStateException(e);
                    }
                });
    }

    /**
     * Naive Row to Object transformation
     */
    @SuppressWarnings("unchecked")
    private T instantiate(ResolvableType elementType, Row row) throws ReflectiveOperationException {
        Object instance = Objects.requireNonNull(elementType.getRawClass()).newInstance();
        Map<String, Object> intermediate = mapper.convertValue(instance, new TypeReference<Map<String, Object>>() {});
        for (String fieldName : intermediate.keySet()) {
            if (row.columnExists(fieldName)) {
                intermediate.put(fieldName, row.cell(fieldName).asString());
            }
        }

        return (T) mapper.convertValue(intermediate, elementType.getRawClass());
    }
}
