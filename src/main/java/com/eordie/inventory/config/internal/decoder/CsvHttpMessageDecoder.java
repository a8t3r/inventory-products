package com.eordie.inventory.config.internal.decoder;

import com.coreoz.windmill.imports.Parsers;
import com.coreoz.windmill.imports.parsers.csv.CsvParserConfig;
import com.eordie.inventory.config.internal.CustomMediaTypes;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.nio.charset.Charset;

public class CsvHttpMessageDecoder<T> extends AbstractRowDecoder<T> {

    public CsvHttpMessageDecoder(ObjectMapper mapper) {
        super(Parsers.csv(CsvParserConfig.builder()
                .charset(Charset.defaultCharset())
                .separator(';')
                .build()),
                mapper,
                CustomMediaTypes.TEXT_CSV);
    }
}
