package com.eordie.inventory.config.internal.decoder;

import com.coreoz.windmill.imports.Parsers;
import com.eordie.inventory.config.internal.CustomMediaTypes;
import com.fasterxml.jackson.databind.ObjectMapper;

public class XlsHttpMessageDecoder<T> extends AbstractRowDecoder<T> {

    public XlsHttpMessageDecoder(ObjectMapper mapper) {
        super(Parsers.xls(), mapper, CustomMediaTypes.APPLICATION_EXCEL);
    }
}
