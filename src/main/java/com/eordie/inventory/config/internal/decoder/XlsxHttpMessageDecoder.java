package com.eordie.inventory.config.internal.decoder;

import com.coreoz.windmill.imports.Parsers;
import com.eordie.inventory.config.internal.CustomMediaTypes;
import com.fasterxml.jackson.databind.ObjectMapper;

public class XlsxHttpMessageDecoder<T> extends AbstractRowDecoder<T> {

    public XlsxHttpMessageDecoder(ObjectMapper mapper) {
        super(Parsers.xlsx(), mapper, CustomMediaTypes.APPLICATION_EXCEL_X);
    }
}
