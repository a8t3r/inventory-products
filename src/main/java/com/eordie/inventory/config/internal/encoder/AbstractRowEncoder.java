package com.eordie.inventory.config.internal.encoder;

import com.coreoz.windmill.exports.config.ExportColumn;
import com.coreoz.windmill.exports.config.ExportHeaderMapping;
import com.eordie.inventory.model.commons.Page;
import org.reactivestreams.Publisher;
import org.springframework.beans.BeanUtils;
import org.springframework.core.ResolvableType;
import org.springframework.core.codec.AbstractEncoder;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.util.MimeType;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

abstract class AbstractRowEncoder<T> extends AbstractEncoder<T> {

    AbstractRowEncoder(MimeType... supportedMimeTypes) {
        super(supportedMimeTypes);
    }

    @Override
    public Flux<DataBuffer> encode(Publisher<? extends T> inputStream, DataBufferFactory bufferFactory, ResolvableType elementType, MimeType mimeType, Map<String, Object> hints) {
        DataBuffer dataBuffer = bufferFactory.allocateBuffer();

        ReactiveExporter<T> exporter = buildExporter(elementType, dataBuffer);
        exporter.writeHeaderRow();

        Flux<? extends T> source;
        if (inputStream instanceof Mono && elementType.getRawClass() != null && elementType.getRawClass().isAssignableFrom(Page.class)) {
            source = Mono.from(inputStream)
                .flatMapMany(it -> Flux.fromIterable(((Page<T, ?>) it).getItems()));
        } else {
            source = Flux.from(inputStream);
        }

        return source
                .doOnNext(exporter::writeRow)
                .doOnComplete(() -> exporter.writeTo(dataBuffer.asOutputStream()))
                .thenMany(Flux.just(dataBuffer));
    }

    ExportHeaderMapping<T> getExportHeaders(ResolvableType elementType) {
        Class<?> targetClass = (elementType.hasGenerics() ?
                elementType.getGeneric(0).getRawClass() :
                elementType.getRawClass());

        List<ExportColumn<T>> columns = Arrays.stream(BeanUtils.getPropertyDescriptors(targetClass))
                .filter(descriptor -> descriptor.getPropertyType() != Class.class)
                .map(descriptor -> ExportColumn.of(descriptor.getName(), (T it) -> {
                    try {
                        return descriptor.getReadMethod().invoke(it);
                    } catch (IllegalArgumentException | ReflectiveOperationException e) {
                        throw new IllegalStateException(e);
                    }
                }))
                .collect(Collectors.toList());

        return new ExportHeaderMapping<>(columns);
    }

    abstract ReactiveExporter<T> buildExporter(ResolvableType elementType, DataBuffer dataBuffer);
}
