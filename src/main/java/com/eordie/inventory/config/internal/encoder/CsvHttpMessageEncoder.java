package com.eordie.inventory.config.internal.encoder;

import com.coreoz.windmill.exports.exporters.csv.ExportCsvConfig;
import com.eordie.inventory.config.internal.CustomMediaTypes;
import org.springframework.core.ResolvableType;
import org.springframework.core.io.buffer.DataBuffer;

import java.nio.charset.Charset;

public class CsvHttpMessageEncoder<T> extends AbstractRowEncoder<T> {

    private ExportCsvConfig config;

    public CsvHttpMessageEncoder() {
        super(CustomMediaTypes.TEXT_CSV);
        this.config = ExportCsvConfig.builder()
                .charset(Charset.defaultCharset())
                .separator(';')
                .build();
    }

    @Override
    ReactiveExporter<T> buildExporter(ResolvableType elementType, DataBuffer dataBuffer) {
        return new CsvReactiveExporter<>(config, dataBuffer.asOutputStream(), getExportHeaders(elementType));
    }
}
