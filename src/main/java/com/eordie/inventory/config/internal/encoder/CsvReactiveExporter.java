package com.eordie.inventory.config.internal.encoder;

import com.coreoz.windmill.exports.config.ExportHeaderMapping;
import com.coreoz.windmill.exports.exporters.csv.ExportCsvConfig;
import com.opencsv.CSVWriter;
import lombok.SneakyThrows;
import org.apache.commons.lang3.ObjectUtils;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

public class CsvReactiveExporter<T> implements ReactiveExporter<T> {

    private final ExportCsvConfig config;
    private final CSVWriter writer;
    private final ExportHeaderMapping<T> mapping;

    public CsvReactiveExporter(ExportCsvConfig config, OutputStream outputStream, ExportHeaderMapping<T> mapping) {
        this.config = config;
        this.writer = buildWriter(outputStream);
        this.mapping = mapping;
    }

    private CSVWriter buildWriter(OutputStream outputStream) {
        return new CSVWriter(
                new OutputStreamWriter(outputStream, config.getCharset()),
                config.getSeparator(),
                config.getQuoteChar(),
                config.getEscapeChar(),
                config.getLineEnd()
        );
    }

    @Override
    public void writeHeaderRow() {
        List<String> headerColumn = mapping.headerColumns();
        if(!headerColumn.isEmpty()) {
            String[] csvRowValues = new String[headerColumn.size()];
            for (int i = 0; i < headerColumn.size(); i++) {
                csvRowValues[i] = ObjectUtils.defaultIfNull(headerColumn.get(i), "");
            }

            writer.writeNext(csvRowValues, config.isApplyQuotesToAll());
        }
    }

    @Override
    @SneakyThrows
    public void writeRow(T row) {
        String[] csvRowValues = new String[mapping.columnsCount()];
        for (int i = 0; i < mapping.columnsCount(); i++) {
            csvRowValues[i] = String.valueOf(ObjectUtils.defaultIfNull(mapping.cellValue(i, row), ""));
        }

        writer.writeNext(csvRowValues, config.isApplyQuotesToAll());
        writer.flush();
    }

    @Override
    public OutputStream writeTo(OutputStream outputStream) {
        return outputStream;
    }
}
