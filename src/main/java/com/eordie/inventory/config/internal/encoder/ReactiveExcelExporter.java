package com.eordie.inventory.config.internal.encoder;

import com.coreoz.windmill.exports.config.ExportMapping;
import com.coreoz.windmill.exports.exporters.excel.ExcelCellStyler;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RequiredArgsConstructor
public class ReactiveExcelExporter<T> implements ReactiveExporter<T> {

    private final ExportMapping<T> mapping;
    private final Sheet sheet;
    private final ExcelCellStyler styler;

    public ReactiveExcelExporter(ExportMapping<T> mapping, Workbook workbook) {
        this(mapping, workbook.createSheet(), ExcelCellStyler.bordersStyle());
    }

    private Row currentExcelRow;

    @Override
    public void writeHeaderRow() {
        List<String> headerColumn = mapping.headerColumns();
        if(!headerColumn.isEmpty()) {
            initializeExcelRow();
            for (int i = 0; i < headerColumn.size(); i++) {
                setCellValue(headerColumn.get(i), i);
            }
        }
    }

    @Override
    public void writeRow(T row) {
        initializeExcelRow();
        for (int i = 0; i < mapping.columnsCount(); i++) {
            setCellValue(mapping.cellValue(i, row), i);
        }
    }

    private void initializeExcelRow() {
        int rowIndex = currentExcelRow == null ? 0 : currentExcelRow.getRowNum() + 1;
        currentExcelRow = sheet.getRow(rowIndex);
        if(currentExcelRow == null) {
            currentExcelRow =  sheet.createRow(rowIndex);
        }
    }

    @Override
    @SneakyThrows
    public OutputStream writeTo(OutputStream outputStream) {
        sheet.getWorkbook().write(outputStream);
        return outputStream;
    }

    private void setCellValue(final Object value, final int columnIndex) {
        Cell cell = currentExcelRow.getCell(
                columnIndex,
                Row.MissingCellPolicy.CREATE_NULL_AS_BLANK
        );

        styler.style(cell);

        if(value == null) {
            return;
        }

        // numbers
        if (value instanceof Integer) {
            cell.setCellValue(Double.valueOf(((Integer) value).intValue()));
        } else if (value instanceof Long) {
            cell.setCellValue(Double.valueOf(((Long) value).longValue()));
        } else if (value instanceof Float) {
            cell.setCellValue(Double.valueOf(((Float) value).floatValue()));
        } else if (value instanceof BigDecimal) {
            cell.setCellValue(((BigDecimal) value).doubleValue());
        } else if (value instanceof Double) {
            cell.setCellValue((Double) value);
        }
        // other types
        else if (value instanceof Boolean) {
            cell.setCellValue(((Boolean) value).booleanValue());
        } else if (value instanceof Calendar) {
            cell.setCellValue((Calendar) value);
        } else if (value instanceof Date) {
            cell.setCellValue((Date) value);
        } else if (value instanceof String) {
            cell.setCellValue((String) value);
        }

        else {
            cell.setCellValue(value.toString());
        }
    }
}
