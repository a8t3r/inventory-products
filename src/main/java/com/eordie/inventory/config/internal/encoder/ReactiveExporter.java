package com.eordie.inventory.config.internal.encoder;

import java.io.OutputStream;

/**
 * By default, csv/xls library doesn't support reactive stream processing
 * This interface is a sort of an adapter for reactive support
 */
public interface ReactiveExporter<T> {

    void writeHeaderRow();

    void writeRow(T row);

    OutputStream writeTo(OutputStream outputStream);

}
