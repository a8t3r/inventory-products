package com.eordie.inventory.config.internal.encoder;

import com.eordie.inventory.config.internal.CustomMediaTypes;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.core.ResolvableType;
import org.springframework.core.io.buffer.DataBuffer;

public class XlsHttpMessageEncoder<T> extends AbstractRowEncoder<T> {

    public XlsHttpMessageEncoder() {
        super(CustomMediaTypes.APPLICATION_EXCEL);
    }

    @Override
    ReactiveExporter<T> buildExporter(ResolvableType elementType, DataBuffer dataBuffer) {
        return new ReactiveExcelExporter<>(getExportHeaders(elementType), new HSSFWorkbook());
    }
}
