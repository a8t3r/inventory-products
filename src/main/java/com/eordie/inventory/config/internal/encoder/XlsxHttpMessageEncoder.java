package com.eordie.inventory.config.internal.encoder;

import com.eordie.inventory.config.internal.CustomMediaTypes;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.ResolvableType;
import org.springframework.core.io.buffer.DataBuffer;

public class XlsxHttpMessageEncoder<T> extends AbstractRowEncoder<T> {

    public XlsxHttpMessageEncoder() {
        super(CustomMediaTypes.APPLICATION_EXCEL_X);
    }

    @Override
    ReactiveExporter<T> buildExporter(ResolvableType elementType, DataBuffer dataBuffer) {
        return new ReactiveExcelExporter<>(getExportHeaders(elementType), new XSSFWorkbook());
    }
}
