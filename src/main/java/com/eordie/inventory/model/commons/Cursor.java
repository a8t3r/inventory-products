package com.eordie.inventory.model.commons;

import java.time.LocalDateTime;

public interface Cursor {

    int getPageSize();

    void moveWindow(LocalDateTime dateTime);

}
