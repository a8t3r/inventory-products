package com.eordie.inventory.model.commons;

import java.time.LocalDateTime;

public interface Navigable {

    LocalDateTime cursor();

}
