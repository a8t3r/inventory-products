package com.eordie.inventory.model.commons;

import com.eordie.inventory.utils.Base64Serializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Page<T, C> {

    @Data
    public static class Paging<C> {
        @JsonSerialize(using = Base64Serializer.class)
        private C nextCursor;

    }

    private Collection<T> items;
    private Paging<C> paging;

    public static <C extends Cursor, T extends Navigable> Mono<Page<T, C>> of(C actualCursor, Flux<T> items) {
        return items.collectList()
             .map(list -> {
                 C nextCursor = null;
                 if (!list.isEmpty() && list.size() == actualCursor.getPageSize()) {
                     T element = list.get(list.size() - 1);
                     nextCursor = actualCursor;
                     nextCursor.moveWindow(element.cursor());
                 }

                 Paging<C> paging = new Paging<>();
                 paging.setNextCursor(nextCursor);
                 return new Page<>(list, paging);
             });
    }
}
