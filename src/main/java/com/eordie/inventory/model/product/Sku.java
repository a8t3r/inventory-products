package com.eordie.inventory.model.product;

import com.eordie.inventory.model.commons.Navigable;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Sku extends SkuProto implements Navigable {

    @NotNull
    private UUID id;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public LocalDateTime createdAt = LocalDateTime.now();

    public Sku(UUID id, SkuProto proto) {
        this.id = id;
        setName(proto.getName());
        setBrand(proto.getBrand());
        setTareCapacity(proto.getTareCapacity());
        setTare(proto.getTare());
        setQuantity(proto.getQuantity());
        setPrice(proto.getPrice());
    }

    @Override
    public LocalDateTime cursor() {
        return createdAt;
    }
}
