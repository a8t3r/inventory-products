package com.eordie.inventory.model.product;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Data
public class SkuProto {

    public interface Update {}

    @NotBlank
    @Length(max = 256, groups = Update.class)
    private String name;

    @Length(max = 256, groups = Update.class)
    private String brand;

    @NotNull
    @PositiveOrZero(groups = Update.class)
    private BigDecimal price;

    private Tare tare = Tare.milliliter;

    @Positive(groups = Update.class)
    private BigDecimal tareCapacity = BigDecimal.ONE;

    private BigDecimal quantity = BigDecimal.ZERO;

}
