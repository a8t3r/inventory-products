package com.eordie.inventory.model.product;

public enum Tare {

    milliliter,
    liter,
    gram

}
