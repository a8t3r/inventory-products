package com.eordie.inventory.service;

import com.eordie.inventory.api.SkuCursor;
import com.eordie.inventory.model.commons.Page;
import com.eordie.inventory.model.product.Sku;
import com.eordie.inventory.model.product.Tare;
import com.github.pgasync.Db;
import com.github.pgasync.Row;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.UUID;
import java.util.function.Function;

import static rx.RxReactiveStreams.toPublisher;

@Service
@RequiredArgsConstructor
public class SkuService {

    private static final Function<Row, Sku> mapper = row -> {
        Sku result = new Sku();
        result.setId(UUID.fromString(row.getString("id")));
        result.setName(row.getString("name"));
        result.setBrand(row.getString("brand"));
        result.setPrice(row.getBigDecimal("price"));
        result.setCreatedAt(row.get("created_at", LocalDateTime.class));
        result.setQuantity(row.getBigDecimal("quantity"));
        result.setTare(Tare.valueOf(row.getString("tare")));
        result.setTareCapacity(row.getBigDecimal("tare_capacity"));

        return result;
    };

    private final Db db;

    public Mono<Page<Sku, SkuCursor>> findPage(SkuCursor cursor) {
        Flux<Sku> source = Flux.from(toPublisher(db.queryRows("select * from skus " +
                "where (name = $1 or $1 is null) and (brand = $2 or $2 is null) and (quantity <= $3 or $3 is null) and created_at > $4" +
                "order by created_at limit " + cursor.getPageSize(), cursor.getName(), cursor.getBrand(), cursor.getQuantityLte(), cursor.getCreatedAt())
                .map(mapper::apply)));

        return Page.of(cursor, source);
    }

    public Mono<Void> delete(UUID id) {
        return Mono.from(toPublisher(db.queryRows("delete from skus where id = $1", id)))
                .then(Mono.empty());
    }

    public Mono<Sku> findOne(UUID id) {
        return Mono.from(toPublisher(db.queryRows("select * from skus where id = $1", id)
                .map(mapper::apply)));
    }

    public Mono<Sku> updateOne(Mono<Sku> input) {
        return Flux.from(input)
                   .flatMap(it -> toPublisher(db.queryRows("update skus set " +
                                   "name = coalesce($2, name), " +
                                   "brand = coalesce($3, brand), " +
                                   "tare = coalesce($4, tare), " +
                                   "tare_capacity = coalesce($5, tare_capacity), " +
                                   "price = coalesce($6, price), " +
                                   "quantity = coalesce($7, quantity) " +
                                   "where id = $1 returning *",
                           it.getId(), it.getName(), it.getBrand(), it.getTare().name(), it.getTareCapacity(), it.getPrice(), it.getQuantity())))
                   .map(mapper)
                   .singleOrEmpty();
    }

    public Flux<Sku> save(Flux<Sku> input) {
        return input.flatMap(it -> toPublisher(
                        db.queryRows("insert into skus(id, name, brand, tare, tare_capacity, price, quantity, created_at) " +
                                        "values($1, $2, $3, $4::tare_value, $5, $6, $7, $8)" +
                                        "on conflict(id) do update set name = $2, brand = $3, tare = $4, tare_capacity = $5, price = $6, quantity = $7 " +
                                        "returning *",
                            it.getId(), it.getName(), it.getBrand(), it.getTare().name(), it.getTareCapacity(), it.getPrice(), it.getQuantity(), it.getCreatedAt())))
                    .map(mapper);
    }
}
