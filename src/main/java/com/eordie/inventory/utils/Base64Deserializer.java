package com.eordie.inventory.utils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Base64;
import java.util.Optional;

import static com.eordie.inventory.config.ContentTypeNegotiationConfig.DEFAULT_MAPPER;

public class Base64Deserializer extends JsonDeserializer<Object> implements ContextualDeserializer {

    private Class<?> resultClass;

    @Override
    public JsonDeserializer<?> createContextual(DeserializationContext context, BeanProperty property) {
        this.resultClass = property.getType().getRawClass();
        return this;
    }

    @Override
    public Object deserialize(JsonParser parser, DeserializationContext context) throws IOException {
        String value = parser.getValueAsString();
        try {
            Base64.Decoder decoder = Base64.getDecoder();
            byte[] decodedValue = decoder.decode(value);

            return DEFAULT_MAPPER.readValue(decodedValue, this.resultClass);
        } catch (IllegalArgumentException | JsonParseException e) {
            String fieldName = parser.getParsingContext().getCurrentName();
            Class<?> wrapperClass = parser.getParsingContext().getCurrentValue().getClass();

            throw new InvalidFormatException(
                    parser,
                    String.format("Value for '%s' is not a base64 encoded JSON", fieldName),
                    value,
                    wrapperClass
            );
        }
    }

    public static  <T> Optional<T> deserialize(String value, Class<T> targetClass) {
        if (StringUtils.isBlank(value)) {
            return Optional.empty();
        } else {
            Base64.Decoder decoder = Base64.getDecoder();
            byte[] decodedValue = decoder.decode(value);

            try {
                T read = DEFAULT_MAPPER.readValue(decodedValue, targetClass);
                return Optional.ofNullable(read);
            } catch (IOException e) {
                return Optional.empty();
            }
        }
    }
}
