package com.eordie.inventory.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Base64;

import static com.eordie.inventory.config.ContentTypeNegotiationConfig.DEFAULT_MAPPER;

public class Base64Serializer extends JsonSerializer<Object> {

    @Override
    public void serialize(Object value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        Base64.Encoder encoder = Base64.getEncoder();

        byte[] bytes = DEFAULT_MAPPER.writeValueAsBytes(value);
        String encoded = encoder.encodeToString(bytes);

        gen.writeString(encoded);
    }
}
