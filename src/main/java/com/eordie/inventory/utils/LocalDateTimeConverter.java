package com.eordie.inventory.utils;

import com.github.pgasync.Converter;
import com.github.pgasync.impl.Oid;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_TIME;

public class LocalDateTimeConverter implements Converter<LocalDateTime> {

    private final DateTimeFormatter TIMESTAMP_FORMAT = new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .append(ISO_LOCAL_DATE)
            .appendLiteral(' ')
            .append(ISO_LOCAL_TIME)
            .toFormatter();

    @Override
    public Class<LocalDateTime> type() {
        return LocalDateTime.class;
    }

    @Override
    public byte[] from(LocalDateTime o) {
        return TIMESTAMP_FORMAT.format(o).getBytes(UTF_8);
    }

    @Override
    public LocalDateTime to(Oid oid, byte[] value) {
        String time = new String(value, UTF_8);
        return LocalDateTime.parse(time, TIMESTAMP_FORMAT);
    }
}
