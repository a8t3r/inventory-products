create type tare_value as enum ('milliliter', 'liter', 'gram');

create table skus (
  id uuid primary key,
  name varchar(256) not null,
  brand varchar(256),
  tare tare_value not null,
  tare_capacity numeric not null,
  price numeric,
  quantity numeric,
  created_at timestamp without time zone not null
);