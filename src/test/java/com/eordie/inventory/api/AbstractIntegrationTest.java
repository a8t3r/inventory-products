package com.eordie.inventory.api;

import com.github.pgasync.Db;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.reactive.server.WebTestClient;

abstract class AbstractIntegrationTest {

    @Autowired
    protected WebTestClient client;

    @Autowired
    private Db db;

    void truncate() {
        db.queryRows("truncate skus").subscribe();
    }

}
