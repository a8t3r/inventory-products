package com.eordie.inventory.api;

import com.eordie.inventory.integration.IntegrationTest;
import com.eordie.inventory.model.commons.Page;
import com.eordie.inventory.model.product.Sku;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.shaded.com.google.common.io.Resources;

import static org.junit.Assert.*;

@IntegrationTest
@RunWith(SpringRunner.class)
@WithMockUser(authorities = {"sku:read", "sku:write"})
public class SkuControllerCursorTest extends AbstractIntegrationTest {

    @Before
    public void setUp() {
        truncate();
    }

    @Test
    public void should_iterate_over_cursor() throws Exception {
        client.post().uri("/skus")
              .contentType(MediaType.APPLICATION_JSON)
              .syncBody(Resources.toByteArray(Resources.getResource("items_with_same_name.json")))
              .exchange()
              .expectStatus()
              .isOk()
              .expectBody()
              .jsonPath("length()").isEqualTo(5);

        Page<Sku, String> firstPage = client.get().uri("/skus?page_size={page_size}", 3)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(new ParameterizedTypeReference<Page<Sku, String>>() {})
                .returnResult()
                .getResponseBody();

        assertNotNull(firstPage);
        assertThat(firstPage.getItems(), Matchers.hasSize(3));
        assertNotNull(firstPage.getPaging().getNextCursor());

        Page<Sku, String> secondPage = client.get().uri("/skus?cursor={cursor}", firstPage.getPaging().getNextCursor())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(new ParameterizedTypeReference<Page<Sku, String>>() {})
                .returnResult()
                .getResponseBody();

        assertNotNull(secondPage);
        assertThat(secondPage.getItems(), Matchers.hasSize(2));
        assertNull(secondPage.getPaging().getNextCursor());
    }
}
