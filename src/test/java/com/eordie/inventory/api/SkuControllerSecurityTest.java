package com.eordie.inventory.api;

import com.eordie.inventory.integration.IntegrationTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.shaded.com.google.common.io.Resources;

@IntegrationTest
@RunWith(SpringRunner.class)
public class SkuControllerSecurityTest extends AbstractIntegrationTest {

    @Before
    public void setUp() {
        truncate();
    }

    @Test
    @WithMockUser(authorities = {"sku:read", "sku:write"})
    public void should_allow_read_request_for_admin_user() throws Exception {
        client.get().uri("/skus")
                .exchange()
                .expectStatus()
                .isOk();
    }

    @Test
    @WithMockUser(authorities = {"sku:read", "sku:write"})
    public void should_allow_write_request_for_admin_user() throws Exception {
        client.post().uri("/skus")
                .contentType(MediaType.APPLICATION_JSON)
                .syncBody(Resources.toByteArray(Resources.getResource("item.json")))
                .exchange()
                .expectStatus()
                .isOk();
    }

    @Test
    @WithMockUser(authorities = "sku:read")
    public void should_allow_read_request_for_readonly_user() throws Exception {
        client.get().uri("/skus")
                .exchange()
                .expectStatus()
                .isOk();
    }

    @Test
    @WithMockUser(authorities = "sku:read")
    public void should_not_allow_write_request_for_readonly_user() throws Exception {
        client.post().uri("/skus")
                .contentType(MediaType.APPLICATION_JSON)
                .syncBody(Resources.toByteArray(Resources.getResource("item.json")))
                .exchange()
                .expectStatus()
                .isForbidden();
    }

    @Test
    public void should_not_allow_read_request_for_unauthorized() throws Exception {
        client.get().uri("/skus")
                .exchange()
                .expectStatus()
                .isUnauthorized();
    }

    @Test
    public void should_not_allow_write_request_for_unauthorized() throws Exception {
        client.post().uri("/skus")
                .contentType(MediaType.APPLICATION_JSON)
                .syncBody(Resources.toByteArray(Resources.getResource("item.json")))
                .exchange()
                .expectStatus()
                .isUnauthorized();
    }

    @Test
    @WithMockUser(authorities = "sku:nothing")
    public void should_not_allow_read_request_without_authority() throws Exception {
        client.get().uri("/skus")
                .exchange()
                .expectStatus()
                .isForbidden();
    }

    @Test
    @WithMockUser(authorities = "sku:nothing")
    public void should_not_allow_write_request_without_authority() throws Exception {
        client.post().uri("/skus")
                .contentType(MediaType.APPLICATION_JSON)
                .syncBody(Resources.toByteArray(Resources.getResource("item.json")))
                .exchange()
                .expectStatus()
                .isForbidden();
    }
}
