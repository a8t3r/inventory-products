package com.eordie.inventory.api;

import com.eordie.inventory.config.internal.CustomMediaTypes;
import com.eordie.inventory.integration.IntegrationTest;
import com.eordie.inventory.model.product.SkuProto;
import org.apache.commons.io.IOUtils;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.testcontainers.shaded.com.google.common.io.Resources;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static com.eordie.inventory.config.ContentTypeNegotiationConfig.DEFAULT_MAPPER;

@IntegrationTest
@RunWith(SpringRunner.class)
@WithMockUser(authorities = {"sku:read", "sku:write"})
public class SkuControllerTest extends AbstractIntegrationTest {

    @Before
    public void setUp() {
        truncate();
    }

    @Test
    public void should_process_csv_payload() throws Exception {
        createResource(CustomMediaTypes.TEXT_CSV, "payload.csv")
                .expectBody()
                .jsonPath("length()").isEqualTo(3);
    }

    @Test
    public void should_process_xls_payload() throws Exception {
        createResource(CustomMediaTypes.APPLICATION_EXCEL, "payload.xls")
                .expectBody()
                .jsonPath("length()").isEqualTo(3);
    }

    @Test
    public void should_process_xlsx_payload() throws Exception {
        createResource(CustomMediaTypes.APPLICATION_EXCEL_X, "payload.xlsx")
                .expectBody()
                .jsonPath("length()").isEqualTo(3);
    }

    @Test
    public void should_process_single_item_json() throws Exception {
        createResource(MediaType.APPLICATION_JSON, "item.json")
                .expectBody()
                .jsonPath("length()").isEqualTo(1);

        UUID id = new UUID(0, 1);
        client.get().uri("/skus/{id}", id)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("length()").isEqualTo(7);

        client.delete().uri("/skus/{id}", id)
                .exchange()
                .expectStatus()
                .isNoContent();
    }

    @Test
    public void should_find_by_name_filter() throws Exception {
        createResource(MediaType.APPLICATION_JSON, "items_with_same_name.json")
                .expectBody()
                .jsonPath("length()").isEqualTo(5);

        client.get().uri("/skus?name={name}", "first")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("length()").isEqualTo(2)
                .jsonPath("items.length()").isEqualTo(4)
                .jsonPath("paging.length()").isEqualTo(0);
    }

    @Test
    public void should_find_by_brand_filter() throws Exception {
        createResource(MediaType.APPLICATION_JSON, "items_with_same_name.json")
                .expectBody()
                .jsonPath("length()").isEqualTo(5);

        client.get().uri("/skus?brand={name}", "gabbana")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("length()").isEqualTo(2);
    }

    @Test
    public void should_find_by_brand_filter_and_xls_accept_type() throws Exception {
        createResource(MediaType.APPLICATION_JSON, "items_with_same_name.json")
                .expectBody()
                .jsonPath("length()").isEqualTo(5);

        byte[] responseBody = client.get().uri("/skus?brand={name}", "gabbana")
                .accept(MediaType.parseMediaType("application/vnd.ms-excel"))
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .expectBody()
                .returnResult()
                .getResponseBody();

        // ok, let's truncate current state and recreate with xls payload
        truncate();

        client.post().uri("/skus")
                .contentType(CustomMediaTypes.APPLICATION_EXCEL)
                .syncBody(responseBody)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("length()").isEqualTo(2);
    }

    @Test
    public void should_find_by_name_brand_filter() throws Exception {
        createResource(MediaType.APPLICATION_JSON, "items_with_same_name.json")
                .expectBody()
                .jsonPath("length()").isEqualTo(5);

        client.get().uri("/skus?name={name}&brand={brand}", "first", "dolce")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("length()").isEqualTo(2)
                .jsonPath("items.length()").isEqualTo(3)
                .jsonPath("paging.length()").isEqualTo(0);
    }

    @Test
    public void should_find_by_quantity_filter() throws Exception {
        createResource(MediaType.APPLICATION_JSON, "items_with_same_name.json")
                .expectBody()
                .jsonPath("length()").isEqualTo(5);

        client.get().uri("/skus?quantity[lte]={quantity}", 4)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("length()").isEqualTo(2);
    }

    @Test
    public void should_find_by_quantity_filter_with_csv_accept_type() throws Exception {
        createResource(MediaType.APPLICATION_JSON, "items_with_same_name.json")
                .expectBody()
                .jsonPath("length()").isEqualTo(5);

        byte[] responseBody = client.get().uri("/skus?quantity[lte]={quantity}", 4)
                .accept(MediaType.parseMediaType("text/csv"))
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.parseMediaType("text/csv"))
                .expectBody()
                .returnResult()
                .getResponseBody();

        List<String> lines = IOUtils.readLines(new ByteArrayInputStream(responseBody));
        Assert.assertThat(lines, Matchers.hasSize(3));
    }

    @Test
    public void should_support_create_and_update_lifecycle() throws Exception {
        UUID id = UUID.randomUUID();

        SkuProto updatedFields = new SkuProto();
        updatedFields.setName("foo");
        updatedFields.setPrice(new BigDecimal("10"));

        client.put().uri("/skus/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .syncBody(DEFAULT_MAPPER.writeValueAsBytes(updatedFields))
                .exchange()
                .expectStatus()
                .isOk();

        updatedFields = new SkuProto();
        updatedFields.setBrand("bar");
        updatedFields.setPrice(new BigDecimal("42"));
        updatedFields.setQuantity(new BigDecimal("100500"));

        client.patch().uri("/skus/{id}", id)
              .contentType(MediaType.APPLICATION_JSON)
              .syncBody(DEFAULT_MAPPER.writeValueAsBytes(updatedFields))
              .exchange()
              .expectStatus()
              .isOk()
              .expectBody()
                .jsonPath("brand").isEqualTo("bar")
                .jsonPath("price").isEqualTo(42)
                .jsonPath("quantity").isEqualTo(100500);

        client.get().uri("/skus/{id}", id)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("length()").isEqualTo(8)
                .jsonPath("id").isEqualTo(id.toString())
                .jsonPath("name").isEqualTo("foo")
                .jsonPath("brand").isEqualTo("bar")
                .jsonPath("price").isEqualTo(42)
                .jsonPath("quantity").isEqualTo(100500)
                .jsonPath("tare").isEqualTo("milliliter")
                .jsonPath("tare_capacity").isEqualTo(1)
                .jsonPath("created_at").exists();
    }

    @Test
    public void should_return_not_found_on_unknown_resource() {
        client.get().uri("/skus/{id}", UUID.randomUUID())
              .exchange()
              .expectStatus()
              .isNotFound();
    }

    private WebTestClient.ResponseSpec createResource(MediaType mediaType, String resourceName) throws IOException {
        return client.post().uri("/skus")
              .contentType(mediaType)
              .syncBody(Resources.toByteArray(Resources.getResource(resourceName)))
              .exchange()
              .expectStatus()
              .isOk();
    }
}