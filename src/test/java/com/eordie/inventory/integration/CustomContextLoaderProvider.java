package com.eordie.inventory.integration;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.test.context.MergedContextConfiguration;
import org.testcontainers.containers.PostgreSQLContainer;

public class CustomContextLoaderProvider {

    private static PostgreSQLContainer postgres = new PostgreSQLContainer();

    static {
        postgres.start();
        Runtime.getRuntime().addShutdownHook(new Thread(() -> postgres.stop()));

    }

    public static class CustomContextLoader extends SpringBootContextLoader {
        @Override
        protected String[] getInlinedProperties(MergedContextConfiguration config) {
            return ArrayUtils.addAll(super.getInlinedProperties(config),
                    "spring.datasource.url=" + postgres.getJdbcUrl() + "?stringtype=unspecified",
                    "spring.datasource.username=" + postgres.getUsername(),
                    "spring.datasource.password=" + postgres.getPassword(),
                    "spring.datasource.continueOnError=true",
                    "spring.datasource.initialization-mode=ALWAYS",
                    "spring.jpa.hibernate.ddl-auto=none",
                    "spring.jpa.properties.hibernate.hbm2ddl.auto=none",
                    "spring.cloud.zookeeper.enabled=false");
        }
    }
}

