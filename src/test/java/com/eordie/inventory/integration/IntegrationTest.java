package com.eordie.inventory.integration;

import com.eordie.inventory.Application;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@SpringBootTest(
        classes = {Application.class},
        webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT
)
@ContextConfiguration(
        classes = {TestConfiguration.class},
        loader = CustomContextLoaderProvider.CustomContextLoader.class
)
public @interface IntegrationTest {

}